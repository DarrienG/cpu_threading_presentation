I ran this on my Mac and both the "fast" and "slow" examples seem to be about
the same, what's going on?

On MacOS, gcc != gcc. You're actually compiling the program with clang.

The two work very similarly a lot of the time, but in the cases of conditionals,
they have taken different approaches.

gcc uses real branches, and is affected by CPU branch prediction because of it.

clang on the other hand uses conditional moves, which eliminates the CPUs
ability to do branch prediction, thereby getting middle of the road performance.

You can see a difference in the generated assembly here:
https://godbolt.org/z/CTrC5_

Please see the addendum folder if you'd like to see how to get consistent (but
slow) results using clang - tested with version 10.
