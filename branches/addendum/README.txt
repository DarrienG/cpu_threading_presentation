So you really really want to see the branch prediction with clang?

How we do it is not particularly intuitive as we have to do a lot to outsmart
the compiler.

To make this work we cannot use standard conditionals since clang will still be
too smart and continue to avoid the branch predictor. We must do this instead:

int less_than_five_million = (((arr[i] - FIVE_MILLION) >> 0x1F) & 1);
switch (less_than_five_million) {
  case 0:
    result.greater_half_sum += arr[i];
    break;
  case 1:
    result.less_half_sum += arr[i];
    break;
}

Which reduces the number we received to a 0 if it is greater than 5 million or 1
if it is less than 5 million.

This level of indirection makes it harder for clang to understand what is going
on.

At this point we have stripped enough information from clang that it no longer has
any idea what is going on, and generates (terribly unoptimized) code that works
with the standard branch predictor on Intel processors.

You would never do this in production code because it is always slower in this
case. It is also harder to read than standard conditionals.

What's particularly interesting is that when you do this, the completion times
between Clang and GCC are extremely similar and they both output very similar
code.

It seems that the assumption here is that if you are using bitshifts, you must
have already put in a lot of the work optimizing yourself.

That isn't always the case though ;)
