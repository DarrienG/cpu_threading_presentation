#ifndef STDBG_LIB_H
#define STDBG_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define BILLION 1000000000L

void shuffle(int *arr, size_t size);
void populate_array(int **arr_2d, int row, int col);
long long unsigned int get_time_passed(struct timespec start,
                                       struct timespec end);
void free_array(int **arr_2d, int row);
void initialize_timer(struct timespec *timespec);
void print_time_elapsed(struct timespec start, struct timespec end);

#endif // STDBG_LIB_H
