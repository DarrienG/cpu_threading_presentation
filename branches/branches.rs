mod stdbglib;

use stdbglib::{initialize_timer, print_time_elapsed, shuffle, timespec};

use std::io::{self, Write};

static TEN_MILLION: usize = 10000000;
static FIVE_MILLION: usize = 5000000;

struct Resulter {
    less_half_sum: i64,
    greater_half_sum: i64,
}

fn main() {
    // ########### SORTED TESTING BEGIN ###########
    let size = TEN_MILLION;
    let sorted_array = populate_sorted_array(size);
    let unsorted_array = populate_unsorted_array(size);

    let mut start = timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };
    let mut end = timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };
    unsafe {
        initialize_timer(&mut start);
    }

    let result = sum_it_by_halves(&sorted_array);
    unsafe {
        initialize_timer(&mut end);
    }
    print!(
        "sorted\t\t| lt: {} - gt: {}\t",
        result.less_half_sum, result.greater_half_sum,
    );
    io::stdout().flush().unwrap();
    unsafe {
        print_time_elapsed(start, end);
    }

    // ########### UNSORTED TESTING BEGIN ###########
    unsafe {
        initialize_timer(&mut start);
    }

    let result = sum_it_by_halves(&unsorted_array);
    unsafe {
        initialize_timer(&mut end);
    }
    print!(
        "unsorted\t| lt: {} - gt: {}\t",
        result.less_half_sum, result.greater_half_sum,
    );
    io::stdout().flush().unwrap();
    unsafe {
        print_time_elapsed(start, end);
    }
}

fn populate_sorted_array(size: usize) -> Vec<i32> {
    let mut arr: Vec<i32> = vec![0; size];
    for i in 0..size {
        arr[i] = i as i32;
    }
    arr
}

fn populate_unsorted_array(size: usize) -> Vec<i32> {
    let mut arr = populate_sorted_array(size);
    unsafe {
        shuffle(arr.as_mut_ptr(), size as i64);
    }
    arr
}

fn sum_it_by_halves(arr: &Vec<i32>) -> Resulter {
    let mut result = Resulter {
        less_half_sum: 0,
        greater_half_sum: 0,
    };
    for i in 0..arr.len() {
        if arr[i] < FIVE_MILLION as i32 {
            result.less_half_sum += arr[i] as i64;
        } else {
            result.greater_half_sum += arr[i] as i64;
        }
    }

    result
}
