#include "stdbglib.h"

#include <stdio.h>
#include <stdlib.h>

void populate_sorted_array(int* arr, int size);
void populate_unsorted_array(int* arr, int size);
struct resulter sum_it_by_halves(int* arr, int size);

static const int TEN_MILLION = 10000000;
static const int FIVE_MILLION = 5000000;

struct resulter {
  long less_half_sum;
  long greater_half_sum;
};

int main(int argc, char* argv[]) {
  int size = TEN_MILLION;
  int* sorted_array = malloc(sizeof(int) * size);
  int* unsorted_array = malloc(sizeof(int) * size);

  populate_sorted_array(sorted_array, size);
  populate_unsorted_array(unsorted_array, size);

  struct timespec start, end;
  struct resulter result;

  initialize_timer(&start);
  result = sum_it_by_halves(sorted_array, size);
  initialize_timer(&end);
  printf("sorted\t\t| lt: %ld - gt: %ld\t", result.less_half_sum, result.greater_half_sum);
  print_time_elapsed(start, end);

  initialize_timer(&start);
  result = sum_it_by_halves(unsorted_array, size);
  initialize_timer(&end);
  printf("unsorted\t| lt: %ld - gt: %ld\t", result.less_half_sum, result.greater_half_sum);
  print_time_elapsed(start, end);

  free(sorted_array);
  free(unsorted_array);
  return 0;
}

void populate_sorted_array(int *arr, int size) {
  for (int i = 0; i < size; ++i) {
    arr[i] = i;
  }
}

void populate_unsorted_array(int *arr, int size) {
  populate_sorted_array(arr, size);
  shuffle(arr, size);
}

struct resulter sum_it_by_halves(int * arr, int size) {
  struct resulter result = { 0, 0 };
  for (int i = 0; i < size; ++i) {
    if (arr[i] < FIVE_MILLION) {
      result.less_half_sum += arr[i];
    } else {
      result.greater_half_sum += arr[i];
    }
  }

  return result;
}
