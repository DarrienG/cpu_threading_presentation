#include <pthread.h>
#include <stdio.h>

#include "stdbglib.h"

void benchmark(int row, int col);
long single_thread_addition(int** arr_2d, int row, int col);
long multi_thread_addition(int ** arr_2d, int row, int col);
void* thread_adder(void* row);

struct row_data {
  int row_len;
  int* row;
  long result;
};

const static int MAX_THREADS = 4;

int main(int argc, char* argv[]) {
  benchmark(10000, 10000);
  benchmark(1000, 1000);
  benchmark(100, 100);
  benchmark(10, 10);
}

void benchmark(int row, int col) {

  int** arr_2d = allocate_2d_array(row, col);
  populate_array(arr_2d, row, col);

  struct timespec start, end;

  printf("**ROWS: %d | COLUMNS: %d**{\n", row, col);
  initialize_timer(&start);
  printf("\tsingl_thread: %ld\t", single_thread_addition(arr_2d, row, col));
  initialize_timer(&end);
  print_time_elapsed(start, end);

  initialize_timer(&start);
  printf("\tmulti_thread: %ld\t", multi_thread_addition(arr_2d, row, col));
  initialize_timer(&end);
  print_time_elapsed(start, end);
  printf("}\n");

  free_array(arr_2d, row);
}

long single_thread_addition(int ** arr_2d, int row, int col) {
  long counter = 0;

  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      counter += arr_2d[i][j];
    }
  }

  return counter;
}

long multi_thread_addition(int ** arr_2d, int row, int col) {
  long counter = 0;
  int i = 0;

  pthread_t threads[MAX_THREADS];
  struct row_data datas[MAX_THREADS];

  while(i < row) {
    int threads_spawned = 0;
    for (; threads_spawned < MAX_THREADS && i + threads_spawned < row; ++threads_spawned, ++i) {
      struct row_data data = {col, arr_2d[i]};
      datas[threads_spawned] = data;
      pthread_create(&threads[threads_spawned], NULL, thread_adder, (void*)&datas[threads_spawned]);
    }
    for (int j = 0; j < threads_spawned; ++j) {
      pthread_join(threads[j], NULL);
      counter += datas[j].result;
    }
  }

  return counter;
}

void* thread_adder(void* data) {
  struct row_data* coerced_data = (struct row_data*)data;
  long thread_counter = 0;
  for (int i = 0; i< coerced_data->row_len; ++i) {
    thread_counter += coerced_data->row[i];
  }

  coerced_data->result = thread_counter;

  return NULL;
}
