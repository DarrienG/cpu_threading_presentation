#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "stdbglib.h"

long row_column(int** arr_2d, int row, int col);
long column_row(int** arr_2d, int row, int col);

static const int TEN_THOUSAND = 10000;

int main(int argc, char* argv[]) {
  int row, col;

  row = TEN_THOUSAND;
  col = TEN_THOUSAND;

  int** arr_2d = allocate_2d_array(row, col);
  if (arr_2d == NULL) {
    printf("Error allocating. Maybe not enough memory?");
    return 1;
  }
  populate_array(arr_2d, row, col);

  struct timespec start, end;

  initialize_timer(&start);
  printf("row_column: %ld\t", row_column(arr_2d, row, col));
  initialize_timer(&end);
  print_time_elapsed(start, end);

  initialize_timer(&start);
  printf("column_row: %ld\t", column_row(arr_2d, row, col));
  initialize_timer(&end);
  print_time_elapsed(start, end);

  free_array(arr_2d, row);
  return 0;
}

long row_column(int ** arr_2d, int row, int col) {
  long counter = 0;

  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      counter += arr_2d[i][j];
    }
  }

  return counter;
}

long column_row(int ** arr_2d, int row, int col) {
  long counter = 0;

  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      counter += arr_2d[j][i];
    }
  }

  return counter;
}
