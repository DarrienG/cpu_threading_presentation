#ifndef STDBG_LIB_H
#define STDBG_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BILLION 1000000000L

void populate_array(int **arr_2d, int row, int col);
long long unsigned int get_time_passed(struct timespec start,
                                       struct timespec end);
int **allocate_2d_array(int row, int col);
void free_array(int **arr_2d, int row);
void initialize_timer(struct timespec *timespec);
void print_time_elapsed(struct timespec start, struct timespec end);

int **allocate_2d_array(int row, int col) {
  int **arr_2d = (int **)malloc(row * sizeof(int *));

  for (int i = 0; i < col; ++i) {
    arr_2d[i] = (int *)malloc(col * sizeof(int));
  }

  return arr_2d;
}

void free_array(int **arr_2d, int row) {
  for (int i = 0; i < row; ++i) {
    free(arr_2d[i]);
  }
  free(arr_2d);
}

/**
 * Takes a 2d array and fills it with numbers left to right from the smallest
 * number to the largest number.
 */
void populate_array(int **arr_2d, int row, int col) {
  int global_count = 0;
  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      arr_2d[i][j] = global_count++;
    }
  }
}

void initialize_timer(struct timespec *timespec) {
  clock_gettime(CLOCK_MONOTONIC, timespec);
}

void print_time_elapsed(struct timespec start, struct timespec end) {
  printf("|\telapsed nanoseconds to completion: %ld\n",
         BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec);
}

#endif // STDBG_LIB_H
