A small repo showing some common concepts modern CPUs use for optimization and
the cost of unix syscalls/threads.

Among the folders are 3 examples, each demonstrating a different concept.

addition: The cost of threads even when allocating one thread per core

branches: The cost of fooling branch prediction

stride: The cost of accessing memory in a non-linear way

-------------------------------------------------------------------------------

All examples are self contained and have minimal requirements.

Supported operating systems: Linux or macOS (>=10.12)

Build requirements:
* gcc (c99 compatible, glibc>=2.14)
* make

-------------------------------------------------------------------------------

To run, go into any of the folders with examples and run:

$ make
$ ./executable # where executable is: addition, branches, or stride

-------------------------------------------------------------------------------

Note that this example also contains a rust implementation as well. You can
build and run with:

$ make branchesrs

And run with:

$ ./branchesrs

Build requirements:
* rustc (2018 compatible and up)
* make
